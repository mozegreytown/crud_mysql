<?php
// src/Form/EventListener/AddEmailFieldListener.php
namespace App\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
class EditBookFieldListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData'
        ];
    }

    public function onPreSetData(FormEvent $event):void {
        $date = new \DateTime();
        $data = $event->getData();

        if (!$data) {
            return;
        }

        $data->setEditedAt($date);
    }
}