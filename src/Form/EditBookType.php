<?php

namespace App\Form;

use App\Entity\Book;
use App\EventListener\AddBookFieldListener;
use App\EventListener\EditBookFieldListener;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditBookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('title')
            ->add('summary')
            ->add('price')
            ->add('signed')
            ->add('category')
            ->add('publishedAt', DateType::class)
            ->addEventSubscriber(new EditBookFieldListener())
        ;
    }
     public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}
